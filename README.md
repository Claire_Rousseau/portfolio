# Portfolio

## Description du projet
Ce projet a pour but la création d'un template de portfolio afin de mettre en avant des projets ou des réalistations.
Il a était réalisé dans le cadre d'un examen lors de la première année de bachelor informatique au sein de Campus Academy.

## Installation
Après avoir clôner le projet ou télécharger le dossier, il suffit de double-cliquer sur le fichier index.html (situé à la racine du dossier). Cela ouvrira votre navigateur par défaut, avec la page d'accueil du projet.

## Technologies
* HTML 5
* CSS 3

## Statut du projet
Ce projet est **en cours** de développement.

## Contributeurs
* Claire Rousseau


